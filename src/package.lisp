;; This software is Copyright ©  2022 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(defpackage :sanitize
  (:use
   :cl)
  (:local-nicknames (:a :alexandria))
  (:export
   :*whitespace-elements*
   :*self-closing-elements*
   :define-rules
   :*basic-sanitize*
   :*strips-all-tags-sanitize*
   :*restricted-sanitize*
   :*relaxed-sanitize*
   :sanitize))

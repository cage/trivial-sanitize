;; This software is Copyright ©  2022 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :sanitize)

(a:define-constant +root-tag+ "root-tag" :test #'string=)

(defparameter *whitespace-elements* '("br"
                                      "address"
                                      "article"
                                      "aside"
                                      "blockquote"
                                      "details"
                                      "dialog"
                                      "dd"
                                      "div"
                                      "dl"
                                      "dt"
                                      "fieldset"
                                      "figcaption"
                                      "figure"
                                      "footer"
                                      "form"
                                      "h1"
                                      "h2"
                                      "h3"
                                      "h4"
                                      "h5"
                                      "h6"
                                      "header"
                                      "hgroup"
                                      "hr"
                                      "li"
                                      "main"
                                      "nav"
                                      "ol"
                                      "p"
                                      "pre"
                                      "section"
                                      "table"
                                      "ul"))

(defun whitespace-element-p (name)
  (member name *whitespace-elements* :test #'string-equal))

(defparameter *self-closing-elements* '("area"
                                        "base"
                                        "br"
                                        "col"
                                        "embed"
                                        "hr"
                                        "img"
                                        "input"
                                        "link"
                                        "meta"
                                        "param"
                                        "source"
                                        "track"))

(defun self-closing-element-p (name)
  (member name *self-closing-elements* :test #'string-equal))

(defstruct rules
  (tags)
  (attributes)
  (add-attributes)
  (compress-whitespaces nil)
  (protocols))

(defmacro define-rules (name &key
                               (tags ())
                               (attributes ())
                               (add-attributes ())
                               (compress-whitespaces nil)
                               (protocols ()))
  `(defparameter ,name
     (make-rules :tags                ',tags
                 :attributes          ',attributes
                 :add-attributes      ',add-attributes
                 :compress-whitespaces ',compress-whitespaces
                 :protocols            ',protocols)))

(define-rules *basic-sanitize*
  :tags   ("a" "abbr" "b" "blockquote" "br" "cite" "code" "dd" "dfn" "dl" "dt" "em" "i"
               "kbd" "li" "mark" "ol" "p" "pre" "q" "s" "samp" "small" "strike" "strong"
               "sub" "sup" "time" "u" "ul" "var")
  :attributes (("a"          . ("href"))
               ("abbr"       . ("title"))
               ("blockquote" . ("cite"))
               ("dfn"        . ("title"))
               ("q"          . ("cite"))
               ("time"       . ("datetime" "pubdate")))
  :add-attributes (("a" . (("rel" . "nofollow"))))
  :protocols (("a"           . (("href" . (:ftp :http :https :mailto :relative))))
              ("blockquote"  . (("cite" . (:http :https :relative))))
              ("q"           . (("cite" . (:http :https :relative))))))

(define-rules *strips-all-tags-sanitize*)

(define-rules *restricted-sanitize*
  :tags ("b" "em" "i" "strong" "u"))

(define-rules *relaxed-sanitize*
  :tags ("a" "abbr" "b" "bdo" "blockquote" "br" "caption" "cite" "code" "col"
             "colgroup" "dd" "del" "dfn" "dl" "dt" "em" "figcaption" "figure" "h1" "h2"
             "h3" "h4" "h5" "h6" "hgroup" "i" "img" "ins" "kbd" "li" "mark" "ol" "p" "pre"
             "q" "rp" "rt" "ruby" "s" "samp" "small" "strike" "strong" "sub" "sup" "table"
             "tbody" "td" "tfoot" "th" "thead" "time" "tr" "u" "ul" "var" "wbr" "font")
  :attributes ((:all         . ("dir" "lang" "title" "class"))
               ("a"          . ("href"))
               ("blockquote" . ("cite"))
               ("col"        . ("span" "width"))
               ("colgroup"   . ("span" "width"))
               ("del"        . ("cite" "datetime"))
               ("img"        . ("align" "alt" "height" "src" "width"))
               ("ins"        . ("cite" "datetime"))
               ("ol"         . ("start" "reversed" "type"))
               ("q"          . ("cite"))
               ("table"      . ("summary" "width"))
               ("td"         . ("abbr" "axis" "colspan" "rowspan" "width"))
               ("th"         . ("abbr" "axis" "colspan" "rowspan" "scope" "width"))
               ("time"       . ("datetime" "pubdate"))
               ("ul"         . ("type"))
               ("font"       . ("size")))
  :protocols (("a"           . (("href" . (:ftp :http :https :mailto :relative))))
              ("blockquote"  . (("cite" . (:http :https :relative))))
              ("del"         . (("cite" . (:http :https :relative))))
              ("img"         . (("src"  . (:http :https :relative))))
              ("ins"         . (("cite" . (:http :https :relative))))
              ("q"           . (("cite" . (:http :https :relative))))))

(defun tree-tag (node)
  "Given a node returns the tag part"
  (first node))

(defun tree-attributes (node)
  "Given a node returns the attribute part"
  (second node))

(defun tree-children (node)
  "Return children of this nodes if exists"
  (when (and node
             (listp node)
             (> (length node)
                2))
    (subseq node 2)))

(defstruct node
  name
  attrs
  children
  parent
  whitespace-element-p
  self-closing-element-p)

(defstruct (text-node (:include node))
  text)

(defun node-root-p (node)
  (null (node-parent node)))

;; from hunchentoot
(defun escape-for-html (string)
  "Escapes the characters #\\<, #\\>, #\\', #\\\", and #\\& for HTML
output."
  (with-output-to-string (out)
    (with-input-from-string (in string)
      (loop for char = (read-char in nil nil)
            while char
            do (case char
                 ((#\<) (write-string "&lt;" out))
                 ((#\>) (write-string "&gt;" out))
                 ((#\") (write-string "&quot;" out))
                 ;;((#\') (write-string "&#039;" out))
                 ((#\&) (write-string "&amp;" out))
                 (otherwise (write-char char out)))))))

(defun tree->struct (tree &optional (parent nil))
  (cond
    ((null tree)
     tree)
    ((stringp tree)
     (make-text-node :parent parent :text (escape-for-html tree)))
    (t
     (let* ((name          (tree-tag tree))
            (whitespace-element (whitespace-element-p name))
            (self-closing  (self-closing-element-p name))
            (node (make-node :parent                 parent
                             :whitespace-element-p   whitespace-element
                             :self-closing-element-p self-closing
                             :name                   (tree-tag tree)
                             :attrs                  (tree-attributes tree))))
       (setf (node-children node)
             (when (tree-children tree)
               (loop for child in (tree-children tree)
                     when child
                       collect
                       (tree->struct child node))))
       node))))

(defun remove-non-allowed-tag (rules node match-function)
  (when (not (find (node-name node)
                   (rules-tags rules)
                   :test match-function))
    (setf (node-name node) nil)))

(defun remove-illegal-attributes (rules node match-function)
  (a:when-let ((attributes (append (cdr (assoc (node-name node)
                                               (rules-attributes rules)
                                               :test match-function))
                                   (cdr (assoc :all
                                               (rules-attributes rules)
                                               :test #'eq)))))
    (setf (node-attrs node)
          (remove-if-not (lambda (a)
                           (find (first a)
                                 attributes
                                 :test match-function))
                         (node-attrs node)))))

(defun remove-illegal-protocols (rules node match-function)
  (a:when-let ((attributes (cdr (assoc (node-name node)
                                       (rules-protocols rules)
                                       :test match-function)))
               (node-attrs (node-attrs node)))
    (setf (node-attrs node)
          (remove-if-not (lambda (a)
                           (let ((allowed-protocols (rest (find-if (lambda (b)
                                                                     (funcall match-function
                                                                              (first a)
                                                                              (first b)))
                                                                   attributes)))
                                 (scheme (get-scheme (second a))))
                             (if (or (null scheme)
                                     (null allowed-protocols))
                                 t
                                 (find scheme
                                       allowed-protocols
                                       :test match-function))))
                         node-attrs))))

(defun maybe-add-attribute (rules node match-function)
  (a:when-let ((attributes (cdr (assoc (node-name node)
                                       (rules-add-attributes rules)
                                       :test match-function))))
    (setf (node-attrs node)
          (append (node-attrs node)
                  (loop for attr in attributes
                        collect
                        (list (car attr) (cdr attr)))))))

(defun parse (data strip-comments)
  (multiple-value-bind (tree errors)
      (html5-parser:parse-html5-fragment (format nil
                                                 "<~a>~a</~a>"
                                                 +root-tag+
                                                 data
                                                 +root-tag+)
                                         :dom (list :xmls
                                                    :comments (not strip-comments)))
    (let ((struct (tree->struct (first tree))))
      (values struct
              errors))))

(defun get-scheme (uri)
  (multiple-value-bind (matchedp groups)
      (cl-ppcre:scan-to-strings "\\p{Z}*([a-zA-Z][a-zA-Z0-9+-.]*)\\p{Z}*:\\p{Z}*"
                                uri)
    (when matchedp
      (a:first-elt groups))))

(defun sanitize (str rules
                 &key
                   (case-insensitive-tag-match        t)
                   (strip-comments                    t)
                   (strips-all-tags-on-malformed-html nil))
  (multiple-value-bind (nodes errors)
      (parse str strip-comments)
    (let ((match-fn (if case-insensitive-tag-match
                        #'string-equal
                        #'string=)))
      (labels ((descend-clean (root)
                 (when (not (text-node-p root))
                   (remove-non-allowed-tag rules root match-fn)
                   (remove-illegal-attributes rules root match-fn)
                   (remove-illegal-protocols rules root match-fn)
                   (maybe-add-attribute rules root match-fn)
                   (loop for child in (node-children root)
                         do (descend-clean child))))
               (concat (nodes-tree)
                 (reduce (lambda (a b) (concatenate 'string a b))
                         (a:flatten nodes-tree)
                         :initial-value ""))
               (collect (root pass-space)
                 (loop for child in (node-children root)
                       collect (descend-print child pass-space)))
               (print-tag (node pass-space)
                 (if pass-space
                     (format nil
                             " <~a~@[ ~{~{~a=\"~a\"~}~^ ~}~]>~a</~a> "
                             (node-name node)
                             (node-attrs node)
                             (concat (collect node nil))
                             (node-name node))
                     (format nil
                             "<~a~@[ ~{~{~a=\"~a\"~}~^ ~}~]>~a</~a>"
                             (node-name node)
                             (node-attrs node)
                             (concat (collect node nil))
                             (node-name node))))
               (descend-print (root pass-space)
                 (cond
                   ((text-node-p root)
                    (if pass-space
                        (format nil " ~a " (text-node-text root))
                        (format nil "~a"   (text-node-text root))))
                   ((node-root-p root)
                    (format nil "~a" (concat (collect root nil))))
                   ((node-name root)
                    (if (node-self-closing-element-p root)
                        (format nil "<~a>" (node-name root))
                        (print-tag root pass-space)))
                   ((node-whitespace-element-p root)
                    (if (null (node-children root))
                        (format nil " ")
                        (format nil "~a" (concat (collect root t)))))
                   (t
                    (format nil "~a" (concat (collect root pass-space)))))))
        (descend-clean nodes)
        (let ((clean (descend-print nodes nil)))
          (if (and strips-all-tags-on-malformed-html
                   errors)
              (sanitize clean
                        *strips-all-tags-sanitize*
                        :case-insensitive-tag-match case-insensitive-tag-match
                        :strip-comments             strip-comments)
              clean))))))

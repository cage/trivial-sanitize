;; This software is Copyright ©  2022 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :all-tests)

(defsuite all-suite ())

(defun run-all-tests (&key (use-debugger t))
  (clunit:run-suite 'all-suite :use-debugger use-debugger :report-progress t))

(deftest remove-illegal-tag (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize "<script>foo</script>" *basic-sanitize*)))

(deftest remove-illegal-attribute (all-suite)
  (assert-equality #'string=
      "<a href=\"bar\" rel=\"nofollow\">foo</a>"
      (sanitize "<a abbr=\"rm\" style=\"rm\" href=\"bar\">foo</a>"
                *basic-sanitize*)))

;;; basic

(a:define-constant +basic-html+
  "<b>Lo<!-- comment -->rem</b> <a href=\"pants\" title=\"foo\">ipsum</a> <a href=\"http://foo.com/\"><strong>dolor</strong></a> sit<br/>amet <script>alert(\"hello world\");</script>"
  :test #'string=)

(deftest basic-default (all-suite)
  (assert-equality #'string=
      "Lorem ipsum dolor sit amet alert(&quot;hello world&quot;);"
      (sanitize +basic-html+ *strips-all-tags-sanitize*)))

(deftest basic-restricted (all-suite)
  (assert-equality #'string=
      "<b>Lorem</b> ipsum <strong>dolor</strong> sit amet alert(&quot;hello world&quot;);"
      (sanitize +basic-html+ *restricted-sanitize*)))

(deftest basic-basic (all-suite)
  (assert-equality #'string=
      "<b>Lorem</b> <a href=\"pants\" rel=\"nofollow\">ipsum</a> <a href=\"http://foo.com/\" rel=\"nofollow\"><strong>dolor</strong></a> sit<br>amet alert(&quot;hello world&quot;);"
      (sanitize +basic-html+ *basic-sanitize*)))

(deftest basic-relaxed (all-suite)
  (assert-equality #'string=
      "<b>Lorem</b> <a href=\"pants\" title=\"foo\">ipsum</a> <a href=\"http://foo.com/\"><strong>dolor</strong></a> sit<br>amet alert(&quot;hello world&quot;);"
      (sanitize +basic-html+ *relaxed-sanitize*)))

;;; malformed

(defparameter *malformed-html*
  "Lo<!-- comment -->rem</b> <a href=pants title=\"foo>ipsum <a href=\"http://foo.com/\"><strong>dolor</a></strong> sit<br/>amet <script>alert(\"hello world\");")

(deftest malformed-default (all-suite)
  (assert-equality #'string=
      "Lorem dolor sit amet alert(&quot;hello world&quot;);&lt;/root-tag&gt;"
      (sanitize *malformed-html* *strips-all-tags-sanitize*)))

(deftest malformed-restricted (all-suite)
  (assert-equality #'string=
      "Lorem <strong>dolor</strong> sit amet alert(&quot;hello world&quot;);&lt;/root-tag&gt;"
      (sanitize *malformed-html* *restricted-sanitize*)))

(deftest malformed-basic (all-suite)
  (assert-equality #'string=
      "Lorem <a href=\"pants\" rel=\"nofollow\"><strong>dolor</strong></a> sit<br>amet alert(&quot;hello world&quot;);&lt;/root-tag&gt;"
      (sanitize *malformed-html* *basic-sanitize*)))

(deftest malformed-relaxed (all-suite)
  (assert-equality #'string=
      "Lorem <a href=\"pants\" title=\"foo>ipsum <a href=\"><strong>dolor</strong></a> sit<br>amet alert(&quot;hello world&quot;);&lt;/root-tag&gt;"
      (sanitize *malformed-html* *relaxed-sanitize*)))

(deftest malformed-relaxed-turn-to-restricted (all-suite)
  (assert-equality #'string=
      "Lorem dolor sit amet alert(&quot;hello world&quot;);&lt;/root-tag&gt;"
      (sanitize *malformed-html* *relaxed-sanitize* :strips-all-tags-on-malformed-html t)))

;; ;;; malicious

(a:define-constant +malicious-html+
  "<b>Lo<!-- comment -->rem</b> <a href=\"javascript:pants\" title=\"foo\">ipsum</a> <a href=\"http://foo.com/\"><strong>dolor</strong></a> sit<br/>amet <<foo>script>alert(\"hello world\");</script>"
  :test #'string=)

(deftest malicious-default (all-suite)
  (assert-equality #'string=
      "Lorem ipsum dolor sit amet &lt;script&gt;alert(&quot;hello world&quot;);"
      (sanitize +malicious-html+ *strips-all-tags-sanitize*)))

(deftest malicious-restricted (all-suite)
  (assert-equality #'string=
      "<b>Lorem</b> ipsum <strong>dolor</strong> sit amet &lt;script&gt;alert(&quot;hello world&quot;);"
      (sanitize +malicious-html+ *restricted-sanitize*)))

(deftest malicious-basic (all-suite)
  (assert-equality #'string=
      "<b>Lorem</b> <a rel=\"nofollow\">ipsum</a> <a href=\"http://foo.com/\" rel=\"nofollow\"><strong>dolor</strong></a> sit<br>amet &lt;script&gt;alert(&quot;hello world&quot;);"
      (sanitize +malicious-html+ *basic-sanitize*)))

(deftest malicious-relaxed (all-suite)
  (assert-equality #'string=
      "<b>Lorem</b> <a title=\"foo\">ipsum</a> <a href=\"http://foo.com/\"><strong>dolor</strong></a> sit<br>amet &lt;script&gt;alert(&quot;hello world&quot;);"
      (sanitize +malicious-html+ *relaxed-sanitize*)))

;;;; entity

;;; should translate valid HTML entities

(deftest misc-1 (all-suite)
  (assert-equality #'string=
      "Don't tasé me &amp; bro!"
      (sanitize "Don&apos;t tas&eacute; me &amp; bro!" *basic-sanitize*)))

;;; should translate valid HTML entities while encoding unencoded ampersands

(deftest misc-2 (all-suite)
  (assert-equality #'string=
      "cookies² &amp; ¼ créme"
      (sanitize "cookies&sup2; & &frac14; cr&eacute;me" *basic-sanitize*)))


;;; should never output &apos;

(deftest misc-3 (all-suite)
  (assert-equality #'string=
      "IE6 isn't a real browser"
      (sanitize "<a href='&apos;' class=\"' &#39;\">IE6 isn't a real browser</a>"
                *strips-all-tags-sanitize*)))

;;; should not choke on several instances of the same element in a row

(deftest misc-4 (all-suite)
  (assert-equality #'string=
      ""
      (sanitize "<img src=\"http://www.google.com/intl/en_ALL/images/logo.gif\"><img src=\"http://www.google.com/intl/en_ALL/images/logo.gif\"><img src=\"http://www.google.com/intl/en_ALL/images/logo.gif\"><img src=\"http://www.google.com/intl/en_ALL/images/logo.gif\">"
                *strips-all-tags-sanitize*)))

;;; should surround  the contents  of *whitespace-elements*  with space
;;; characters when removing the element

(deftest add-whitespace (all-suite)
  (assert-equality #'string=
      "foo bar baz"
      (sanitize "foo<div>bar</div>baz" *strips-all-tags-sanitize*))
  (assert-equality #'string=
      "foo bar baz"
      (sanitize "foo<br>bar<br>baz" *strips-all-tags-sanitize*))
  (assert-equality #'string=
      "foo bar baz"
      (sanitize "foo<hr>bar<hr>baz" *strips-all-tags-sanitize*))
  (assert-equality #'string=
      " <b> <a rel=\"nofollow\">foo</a> </b> "
      (sanitize:sanitize "<div><b><div><a>foo</a></div></b></div>"
                         sanitize:*basic-sanitize*)))

(deftest add-whitespace-compress (all-suite)
  (assert-equality #'string=
      "foo bar baz"
      (sanitize "foo<div><div><div><div><div><div>bar</div></div></div></div></div></div>baz" *strips-all-tags-sanitize*))
    (assert-equality #'string=
      "foo bar baz"
      (sanitize "foo<div><div><div><div><div><div><a><i>bar</i></a></div></div></div></div></div></div>baz" *strips-all-tags-sanitize*)))


;;;; tricky

;;; protocol-based JS injection: simple, no spaces

(a:define-constant +js-injection-html-1+
  "<a href=\"javascript:alert(\'XSS\');\">foo</a>"
  :test #'string=)

(deftest js-injection-1-default (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize +js-injection-html-1+ *strips-all-tags-sanitize*)))

(deftest js-injection-1-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize +js-injection-html-1+ *restricted-sanitize*)))

(deftest js-injection-1-basic (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize +js-injection-html-1+ *basic-sanitize*)))

(deftest js-injection-1-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize +js-injection-html-1+ *relaxed-sanitize*)))

;;; protocol-based JS injection: simple, spaces after

(a:define-constant +js-injection-html-2+
  "<a href=\"javascript    :alert(\'XSS\');\">foo</a>"
  :test #'string=)

(deftest js-injection-2-default (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize +js-injection-html-2+ *basic-sanitize*)))

(deftest js-injection-2-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize +js-injection-html-2+ *restricted-sanitize*)))

(deftest js-injection-2-basic (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize +js-injection-html-2+ *strips-all-tags-sanitize*)))

(deftest js-injection-2-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize +js-injection-html-2+ *relaxed-sanitize*)))

;;; protocol-based JS injection: simple, spaces before

(a:define-constant +js-injection-html-3+
  "<a href=\"     javascript:alert(\'XSS\');\">foo</a>"
  :test #'string=)

(deftest js-injection-3-default (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize +js-injection-html-3+ *basic-sanitize*)))

(deftest js-injection-3-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize +js-injection-html-3+ *restricted-sanitize*)))

(deftest js-injection-3-basic (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize +js-injection-html-3+ *strips-all-tags-sanitize*)))

(deftest js-injection-3-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize +js-injection-html-3+ *relaxed-sanitize*)))

;;; protocol-based JS injection: simple, spaces before

(a:define-constant +js-injection-html-4+
  "<a href=\"javascript     :      alert(\'XSS\');\">foo</a>"
  :test #'string=)

(deftest js-injection-4-default (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize +js-injection-html-4+ *basic-sanitize*)))

(deftest js-injection-4-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize +js-injection-html-4+ *restricted-sanitize*)))

(deftest js-injection-4-basic (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize +js-injection-html-4+ *strips-all-tags-sanitize*)))

(deftest js-injection-4-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize +js-injection-html-4+ *relaxed-sanitize*)))

;;; protocol-based JS injection: preceding colon

(defparameter *js-injection-html-5*
  "<a href=\":javascript:alert(\'XSS\');\">foo</a>")

(deftest js-injection-5-default (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-5* *strips-all-tags-sanitize*)))

(deftest js-injection-5-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-5* *restricted-sanitize*)))

(deftest js-injection-5-basic (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize *js-injection-html-5* *basic-sanitize*)))

(deftest js-injection-5-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize *js-injection-html-5* *relaxed-sanitize*)))

;;; protocol-based JS injection: UTF-8 encoding

(defparameter *js-injection-html-6* "<a href=\"javascript&#58;\">foo</a>")

(deftest js-injection-6-default (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-6* *strips-all-tags-sanitize*)))

(deftest js-injection-6-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-6* *restricted-sanitize*)))

(deftest js-injection-6-basic (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize *js-injection-html-6* *basic-sanitize*)))

(deftest js-injection-6-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize *js-injection-html-6* *relaxed-sanitize*)))

;;; protocol-based JS injection: long UTF-8 encoding

(defparameter *js-injection-html-7* "<a href=\"javascript&#0058;\">foo</a>")

(deftest js-injection-7-default (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-7* *strips-all-tags-sanitize*)))

(deftest js-injection-7-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-7* *restricted-sanitize*)))

(deftest js-injection-7-basic (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize *js-injection-html-7* *basic-sanitize*)))

(deftest js-injection-7-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize *js-injection-html-7* *relaxed-sanitize*)))

;;; protocol-based JS injection: long UTF-8 encoding without semicolons

(defparameter *js-injection-html-8* "<a href=&#0000106&#0000097&#0000118&#0000097&#0000115&#0000099&#0000114&#0000105&#0000112&#0000116&#0000058&#0000097&#0000108&#0000101&#0000114&#0000116&#0000040&#0000039&#0000088&#0000083&#0000083&#0000039&#0000041>foo</a>")

(deftest js-injection-8-default (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-8* *strips-all-tags-sanitize*)))

(deftest js-injection-8-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-8* *restricted-sanitize*)))

(deftest js-injection-8-basic (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize *js-injection-html-8* *basic-sanitize*)))

(deftest js-injection-8-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize *js-injection-html-8* *relaxed-sanitize*)))

;;; protocol-based JS injection: hex encoding

(defparameter *js-injection-html-9* "<a href=\"javascript&#x3A;\">foo</a>")

(deftest js-injection-9-default (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-9* *strips-all-tags-sanitize*)))

(deftest js-injection-9-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-9* *restricted-sanitize*)))

(deftest js-injection-9-basic (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize *js-injection-html-9* *basic-sanitize*)))

(deftest js-injection-9-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize *js-injection-html-9* *relaxed-sanitize*)))

;;; protocol-based JS injection: long hex encoding

(defparameter *js-injection-html-10* "<a href=\"javascript&#x003A;\">foo</a>")

(deftest js-injection-10-default (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-10* *strips-all-tags-sanitize*)))

(deftest js-injection-10-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-10* *restricted-sanitize*)))

(deftest js-injection-10-basic (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize *js-injection-html-10* *basic-sanitize*)))

(deftest js-injection-10-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize *js-injection-html-10* *relaxed-sanitize*)))

;;; protocol-based JS injection: hex encoding without semicolons

(defparameter *js-injection-html-11* "<a href=&#x6A&#x61&#x76&#x61&#x73&#x63&#x72&#x69&#x70&#x74&#x3A&#x61&#x6C&#x65&#x72&#x74&#x28&#x27&#x58&#x53&#x53&#x27&#x29>foo</a>")

(deftest js-injection-11-default (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-11* *strips-all-tags-sanitize*)))

(deftest js-injection-11-restricted (all-suite)
  (assert-equality #'string=
      "foo"
      (sanitize *js-injection-html-11* *restricted-sanitize*)))

(deftest js-injection-11-basic (all-suite)
  (assert-equality #'string=
      "<a rel=\"nofollow\">foo</a>"
      (sanitize *js-injection-html-11* *basic-sanitize*)))

(deftest js-injection-11-relaxed (all-suite)
  (assert-equality #'string=
      "<a>foo</a>"
      (sanitize *js-injection-html-11* *relaxed-sanitize*)))

;;; raw-comment

(defparameter *raw-comment-html* "<!-- comment -->Hello")

(deftest raw-comment-default (all-suite)
  (assert-equality #'string=
      "Hello"
      (sanitize *raw-comment-html* *strips-all-tags-sanitize*)))

(deftest raw-comment-restricted (all-suite)
  (assert-equality #'string=
      "Hello"
      (sanitize *raw-comment-html* *restricted-sanitize*)))

(deftest raw-comment-basic (all-suite)
  (assert-equality #'string=
      "Hello"
      (sanitize *raw-comment-html* *basic-sanitize*)))

(deftest raw-comment-relaxed (all-suite)
  (assert-equality #'string=
      "Hello"
      (sanitize *raw-comment-html* *relaxed-sanitize*)))

(deftest raw-comment-relaxed-keep-comments (all-suite)
  (assert-equality #'string=
      " comment Hello"
      (sanitize *raw-comment-html* *relaxed-sanitize* :strip-comments nil)))

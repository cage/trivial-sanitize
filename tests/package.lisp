;; This software is Copyright ©  2022 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(defpackage :all-tests
  (:use :cl
        :clunit
        :sanitize)
  (:local-nicknames (:a :alexandria))
  (:export
   :all-suite
   :run-all-tests))

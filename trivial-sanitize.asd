;; This software is Copyright ©  2022 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(defsystem :trivial-sanitize
  :description "clean html strings: \"<a>foo</a>\" → \"foo\""
  :author      "cage"
  :license     "LLGPL"
  :version     "0.0.1"
  :encoding    :utf-8
  :maintainer  "cage"
  :bug-tracker "https://codeberg.org/cage/trivial-sanitize/issues"
  :serial      t
  :pathname    "src"
  :depends-on (:alexandria
               :cl-ppcre-unicode
               :cl-html5-parser
               :uiop)
  :components ((:file "package")
               (:file "sanitize"))
  :in-order-to ((test-op (test-op :trivial-sanitize-tests))))

;; This software is Copyright ©  2022 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(defsystem :trivial-sanitize-tests
  :description "Test suite for trivial-sanitize."
  :author      "cage"
  :license     "LLGPL"
  :version     "0.0.1"
  :serial      t
  :pathname    "tests"
  :depends-on (:alexandria
               :clunit2
               :uiop
               :trivial-sanitize)
  :components ((:file "package")
               (:file "all-tests"))
  :perform (test-op (op c) (symbol-call :all-tests :run-all-tests)))
